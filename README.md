WEvotes
====

WEvotes is a CouchDB-backed tool for wiki-based voting.
These files are part of the
[Widget:Vote](http://wikieducator.org/Widget:Vote)
infrastructure used in scenario planning within
WikiEducator.

